#!/bin/bash
clear
echo "If you get BUSID : PCI:1:0:0 then Write 1:0:0 in the terminal"
echo ""
printf "Your BusID is : " 
nvidia-xconfig --query-gpu-info | grep 'BusID : ' | cut -d ' ' -f6
read -p "Please enter BUSID: " userInput
if [[ -z "$userInput" ]]; then
   printf '%s\n' "No BUSID entered"
   exit 1
else
	echo "Script will execute in 10 seconds"
	echo "make sure you run the script in tty2 as it will stop and start gdm services"
	sleep 10
	apt-get update 
	apt-get upgrade -y
	apt-get dist-upgrade -y
	apt install linux-headers-$(uname -r) -y
	apt install nvidia-driver nvidia-xconfig -y
	wget https://gitlab.com/root_cj/nvidia-drivers/raw/master/nvidia-blacklists-nouveau.conf -O /etc/modprobe.d/nvidia-blacklists-nouveau.conf
	update-initramfs -u
	echo "testing if nouveau is disabled"
	sleep 2.5
	systemctl stop gdm
	rmmod nouveau
	while true
	do
	
		output=$(lsmod | grep -i nouveau )
	
		if [ -z "$output"]
		then
			echo "nouveau is disabled"
			sleep 5
			modprobe nvidia-drm
			wget https://gitlab.com/root_cj/nvidia-drivers/raw/master/xorg.conf -O xorg.conf
			sed "s/PCI:1:0:0/PCI:$userInput/g" xorg.conf > /etc/X11/xorg.conf
			rm xorg.conf -f
			wget https://gitlab.com/root_cj/nvidia-drivers/raw/master/optimus.desktop -O /usr/share/gdm/greeter/autostart/optimus.desktop
			cp /usr/share/gdm/greeter/autostart/optimus.desktop /etc/xdg/autostart/optimus.desktop
			apt-get install ocl-icd-libopencl1 nvidia-cuda-toolkit
			clear
			echo "nvidia-drivers installed properly Rebooting in 10 seconds"
			sleep 10
			reboot
			exit 1
		else
			echo "For some reason nouveau is still present please add nouveau.modeset=0 to your grub.cfg"
		fi
	done
fi